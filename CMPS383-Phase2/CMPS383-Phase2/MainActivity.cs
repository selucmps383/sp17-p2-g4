﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System.Json;
using System;


namespace CMPS383_Phase2
{
    [Activity(Label = "League Source", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private EditText userInput;
        private Button searchButton;
        private Toast toast;
        private WebRequest webRequest;
        protected override void OnCreate(Bundle bundle)
        {
            ProgressDialog progress = new ProgressDialog(this);
            base.OnCreate(bundle);
            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);            
            //Edit text and button objects       
            userInput = FindViewById<EditText>(Resource.Id.UserInputText);
            searchButton = FindViewById<Button>(Resource.Id.SearchButton);
            //setting the click for search to start new activity and pass id to the next activity
            searchButton.Click += async (sender, e) =>{
                progress.SetMessage("Please Wait. . .");
                progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                progress.Show();

                //string formatter
                string summonerName = userInput.Text.ToLower().Replace(" ", string.Empty);               
                string url = "https://na.api.pvp.net/api/lol/na/v1.4/summoner/by-name/" + summonerName + "?api_key=RGAPI-7da25950-b120-48ce-806d-24cac18661c2";

                //Try to fetch and recieve summoner info, if fails due to 404, catch with a toast object and display "invalid summoner"
                try
                {
                    webRequest = new WebRequest();                    
                    JsonValue json = await webRequest.FetchLeagueAsync(url);
                    string summonerInfo = getSummonerInfo(json, summonerName);
                    //next 3 lines, set up data to be passed to next activity, pass the data, start the activity
                    Intent searchActivity = new Intent(this, typeof(SearchResultsActivity));
                    searchActivity.PutExtra("Summoner Name", summonerInfo);                                        
                    StartActivity(searchActivity);                                      
                    progress.Dismiss();
                }
                catch
                {
                    //toast object that displays message
                    toast = Toast.MakeText(this, String.Format("INVALID SUMMONER"), ToastLength.Long);
                    progress.Dismiss();
                    toast.Show();
                }                
            };
        }
        private string getSummonerInfo(JsonValue json, string input)
        {
            // Extract the array of name/value results for the field name of user input.
            JsonValue searchResults = json[input];
            //sets name
            string name = searchResults["name"];
            //sets id
            int tempId = searchResults["id"];
            string idString = Convert.ToString(tempId);
            //sets summonerlevel
            int tempLevel = searchResults["summonerLevel"];
            string summonerLevel= Convert.ToString(tempLevel);

            return name + "," + idString + "," + summonerLevel + ",";
        }
    }
}

