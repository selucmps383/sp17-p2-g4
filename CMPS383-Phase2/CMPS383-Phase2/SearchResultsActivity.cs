using System;
using RestSharp;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using RestSharp.Deserializers;
using Android.Graphics;
using System.Net;

namespace CMPS383_Phase2
{
    [Activity(Label = "Recent Matches")]
    public class SearchResultsActivity : Activity
    {
        private TextView name;
        private TextView summonerLevel;               
        public LeagueMatches game;
        public GetChampion getChampConverter = new GetChampion();
        private Button[] matchButtonArray = new Button[7];
        private ImageView[] imageArray = new ImageView[7];
        private string[] words;
        string errorMessage = "MATCH NOT FOUND";
        string noMatchFound = "NO MATCHES FOUND";
        string noImageFound = "IMAGE LOAD FAIL";
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            string url = getSummonerNameSetUrl();            
            SetContentView(Resource.Layout.SearchPage);
            displayUserObjects();
            apiRequest(url);
        }
       /* private void getChampionRequest(string input)
        {
            var champClient = new RestClient("https://na.api.pvp.net/api/lol/static-data/na/v1.2/champion/" + input + "?api_key=RGAPI-7da25950-b120-48ce-806d-24cac18661c2");
            var champRequest = new RestRequest(Method.GET);
            champRequest.RequestFormat = RestSharp.DataFormat.Json;
            champClient.ExecuteAsync(champRequest, champResponse =>
            {               
                    RestSharp.Deserializers.JsonDeserializer deserial2 = new JsonDeserializer();
                    champions = deserial2.Deserialize<Champion>(champResponse);              
            });

        }*/
        private string getSummonerNameSetUrl()
        {
            string inputString = Intent.GetStringExtra("Summoner Name") ?? "Not found";
            words = inputString.Split(',');
            return "https://na.api.pvp.net/api/lol/na/v2.2/matchlist/by-summoner/" + words[1] + "?api_key=RGAPI-7da25950-b120-48ce-806d-24cac18661c2";
        }

        private void displayUserObjects()
        {
            name = FindViewById<TextView>(Resource.Id.nameText);
            summonerLevel = FindViewById<TextView>(Resource.Id.levelText);
            matchButtonArray[0] = FindViewById<Button>(Resource.Id.matchOneButton);
            matchButtonArray[1] = FindViewById<Button>(Resource.Id.matchTwoButton);
            matchButtonArray[2] = FindViewById<Button>(Resource.Id.matchThreeButton);
            matchButtonArray[3] = FindViewById<Button>(Resource.Id.matchFourButton);
            matchButtonArray[4] = FindViewById<Button>(Resource.Id.matchFiveButton);
            matchButtonArray[5] = FindViewById<Button>(Resource.Id.matchSixButton);
            matchButtonArray[6] = FindViewById<Button>(Resource.Id.matchSevenButton);         
            imageArray[0] = FindViewById<ImageView>(Resource.Id.imageView1);
            imageArray[1] = FindViewById<ImageView>(Resource.Id.imageView2);
            imageArray[2] = FindViewById<ImageView>(Resource.Id.imageView3);
            imageArray[3] = FindViewById<ImageView>(Resource.Id.imageView4);
            imageArray[4] = FindViewById<ImageView>(Resource.Id.imageView5);
            imageArray[5] = FindViewById<ImageView>(Resource.Id.imageView6);
            imageArray[6] = FindViewById<ImageView>(Resource.Id.imageView7);

            name.Text = words[0];
            summonerLevel.Text = "Level: " + words[2];
        }

        private void apiRequest(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.RequestFormat = RestSharp.DataFormat.Json;
            client.ExecuteAsync(request, response =>
            {
                RunOnUiThread(() =>
                {                   
                    RestSharp.Deserializers.JsonDeserializer deserial = new JsonDeserializer();
                    game = deserial.Deserialize<LeagueMatches>(response);                
                    try
                    {
                        setButtonInfo();
                        setImages();                   
                    }
                    catch
                    {
                        noMatchFoundButtonSet();
                        Toast toast = Toast.MakeText(this, String.Format(noMatchFound), ToastLength.Long);
                        toast.Show();
                    }
                });
            });
        }
        private void setButtonInfo()
        {
            string[] sentence = new string[7];
            for(int i =0; i < sentence.Length; i++)
            {
                sentence[i] = game.matches[i].lane + "\n" + stringFormatter(game.matches[i].queue);
            }
            for (int i = 0; i < matchButtonArray.Length; i++)
            {
                matchButtonArray[i].Text = sentence[i];
            }           
        }
        private void setImages()
        {
            try
            {
                string test;
                for (int i = 0; i < imageArray.Length; i++)
                {
                    test = getChampConverter.champDecode(game.matches[i].champion);
                    var matchImage = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/champion/" + test + ".png");
                    imageArray[i].SetImageBitmap(matchImage);
                }                
            }
            catch
            {
                Toast toast = Toast.MakeText(this, String.Format(noImageFound), ToastLength.Long);
                toast.Show();
            }            
        }
        private void noMatchFoundButtonSet()
        {            
            for (int i = 0; i < matchButtonArray.Length; i++)
            {
                matchButtonArray[i].Text = errorMessage;
            }            
        }       
        private string stringFormatter(string input)
        {
            string stringResult ="";
            string[] splitMe = input.Split('_');
            for(int i = 0; i < splitMe.Length; i++) {
                if (splitMe[i] == "TEAM" || splitMe[i] == "BUILDER")
                splitMe[i] = string.Empty;
            }
            foreach (string element in splitMe)
            {
                stringResult = stringResult + " " + element + " ";
            }
            return stringResult;
        } 
        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;
            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }
            return imageBitmap;
        }
    }  
}