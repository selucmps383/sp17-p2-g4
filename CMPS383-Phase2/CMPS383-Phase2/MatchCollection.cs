﻿using System.Collections.Generic;

public class Match
    {
        public string region { get; set; }
        public string platformId { get; set; }
        public object matchId { get; set; }
        public int champion { get; set; }
        public string queue { get; set; }
        public string season { get; set; }
        public object timestamp { get; set; }
        public string lane { get; set; }
        public string role { get; set; }
    }

    public class LeagueMatches
    {
        public List<Match> matches { get; set; }
        public int startIndex { get; set; }
        public int endIndex { get; set; }
        public int totalGames { get; set; }
    } 