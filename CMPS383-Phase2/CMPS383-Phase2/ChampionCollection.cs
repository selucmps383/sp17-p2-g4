﻿using System.Collections.Generic;

public class Champion
{
    public int id { get; set; }
    public string key { get; set; }
    public string name { get; set; }
    public string title { get; set; }
}

public class GetChampion
{
    public string champDecode(int input)
    {
        if (input == 99)
        {
            return "Lux";
        }
        if (input == 24)
        {
            return "Jax";
        }
        if (input == 37)
        {
            return "Sona";
        }
        if (input == 18)
        {
            return "Tristana";
        }
        if (input == 110)
        {
            return "Varus";
        }
        if (input == 114)
        {
            return "Fiora";
        }
        if (input == 27)
        {
            return "Singed";
        }
        if (input == 223)
        {
            return "TahmKench";
        }
        if (input == 7)
        {
            return "Leblanc";
        }
        if (input == 412)
        {
            return "Thresh";
        }
        if (input == 43)
        {
            return "Karma";
        }
        if (input == 202)
        {
            return "Jhin";
        }
        if (input == 68)
        {
            return "Rumble";
        }
        if (input == 77)
        {
            return "Udyr";
        }
        if (input == 64)
        {
            return "LeeSin";
        }
        if (input == 83)
        {
            return "Yorick";
        }
        if (input == 38)
        {
            return "Kassadin";
        }
        if (input == 15)
        {
            return "Sivir";
        }
        if (input == 21)
        {
            return "MissFortune";
        }
        if (input == 119)
        {
            return "Draven";
        }
        if (input == 157)
        {
            return "Yasuo";
        }
        if (input == 10)
        {
            return "Kayle";
        }
        if (input == 35)
        {
            return "Shaco";
        }
        if (input == 58)
        {
            return "Renekton";
        }
        if (input == 120)
        {
            return "Hecarim";
        }
        if (input == 105)
        {
            return "Fizz";
        }
        if (input == 96)
        {
            return "KogMaw";
        }
        if (input == 57)
        {
            return "Maokai";
        }
        if (input == 127)
        {
            return "Lissandra";
        }
        if (input == 222)
        {
            return "Jinx";
        }
        if (input == 6)
        {
            return "Urgot";
        }
        if (input == 3)
        {
            return "Galio";
        }
        if (input == 80)
        {
            return "Pantheon";
        }
        if (input == 91)
        {
            return "Talon";
        }
        if (input == 41)
        {
            return "Gangplank";
        }
        if (input == 81)
        {
            return "Ezreal";
        }
        if (input == 150)
        {
            return "Gnar";
        }
        if (input == 17)
        {
            return "Teemo";
        }
        if (input == 1)
        {
            return "Annie";
        }
        if (input == 82)
        {
            return "Mordekaiser";
        }
        if (input == 268)
        {
            return "Azir";
        }
        if (input == 85)
        {
            return "Kennen";
        }
        if (input == 92)
        {
            return "Riven";
        }
        if (input == 31)
        {
            return "Chogath";
        }
        if (input == 266)
        {
            return "Aatrox";
        }
        if (input == 78)
        {
            return "Poppy";
        }
        if (input == 163)
        {
            return "Taliyah";
        }
        if (input == 420)
        {
            return "Illaoi";
        }
        if (input == 74)
        {
            return "Heimerdinger";
        }
        if (input == 12)
        {
            return "Alistar";
        }
        if (input == 5)
        {
            return "XinZhao";
        }
        if (input == 236)
        {
            return "Lucian";
        }
        if (input == 106)
        {
            return "Volibear";
        }
        if (input == 113)
        {
            return "Sejuani";
        }
        if (input == 76)
        {
            return "Nidalee";
        }
        if (input == 86)
        {
            return "Garen";
        }
        if (input == 89)
        {
            return "Leona";
        }
        if (input == 238)
        {
            return "Zed";
        }
        if (input == 53)
        {
            return "Blitzcrank";
        }
        if (input == 33)
        {
            return "Rammus";
        }
        if (input == 161)
        {
            return "Velkoz";
        }
        if (input == 51)
        {
            return "Caitlyn";
        }
        if (input == 48)
        {
            return "Trundle";
        }
        if (input == 203)
        {
            return "Kindred";
        }
        if (input == 9)
        {
            return "FiddleSticks";
        }
        if (input == 133)
        {
            return "Quinn";
        }
        if (input == 245)
        {
            return "Ekko";
        }
        if (input == 267)
        {
            return "Nami";
        }
        if (input == 50)
        {
            return "Swain";
        }
        if (input == 44)
        {
            return "Taric";
        }
        if (input == 134)
        {
            return "Syndra";
        }
        if (input == 72)
        {
            return "Skarner";
        }
        if (input == 201)
        {
            return "Braum";
        }
        if (input == 45)
        {
            return "Veigar";
        }
        if (input == 101)
        {
            return "Xerath";
        }
        if (input == 42)
        {
            return "Corki";
        }
        if (input == 111)
        {
            return "Nautilus";
        }
        if (input == 103)
        {
            return "Ahri";
        }
        if (input == 126)
        {
            return "Jayce";
        }
        if (input == 122)
        {
            return "Darius";
        }
        if (input == 23)
        {
            return "Tryndamere";
        }
        if (input == 40)
        {
            return "Janna";
        }
        if (input == 60)
        {
            return "Elise";
        }
        if (input == 67)
        {
            return "Vayne";
        }
        if (input == 63)
        {
            return "Brand";
        }
        if (input == 104)
        {
            return "Graves";
        }
        if (input == 16)
        {
            return "Soraka";
        }
        if (input == 30)
        {
            return "Karthus";
        }
        if (input == 8)
        {
            return "Vladimir";
        }
        if (input == 26)
        {
            return "Zilean";
        }
        if (input == 55)
        {
            return "Katarina";
        }
        if (input == 102)
        {
            return "Shyvana";
        }
        if (input == 19)
        {
            return "Warwick";
        }
        if (input == 115)
        {
            return "Ziggs";
        }
        if (input == 240)
        {
            return "Kled";
        }
        if (input == 121)
        {
            return "Khazix";
        }
        if (input == 2)
        {
            return "Olaf";
        }
        if (input == 4)
        {
            return "TwistedFate";
        }
        if (input == 20)
        {
            return "Nunu";
        }
        if (input == 107)
        {
            return "Rengar";
        }
        if (input == 432)
        {
            return "Bard";
        }
        if (input == 39)
        {
            return "Irelia";
        }
        if (input == 427)
        {
            return "Ivern";
        }
        if (input == 62)
        {
            return "MonkeyKing";
        }
        if (input == 22)
        {
            return "Ashe";
        }
        if (input == 429)
        {
            return "Kalista";
        }
        if (input == 84)
        {
            return "Akali";
        }
        if (input == 254)
        {
            return "Vi";
        }
        if (input == 32)
        {
            return "Amumu";
        }
        if (input == 117)
        {
            return "Lulu";
        }
        if (input == 25)
        {
            return "Morgana";
        }
        if (input == 56)
        {
            return "Nocturne";
        }
        if (input == 131)
        {
            return "Diana";
        }
        if (input == 136)
        {
            return "AurelionSol";
        }
        if (input == 143)
        {
            return "Zyra";
        }
        if (input == 112)
        {
            return "Viktor";
        }
        if (input == 69)
        {
            return "Cassiopeia";
        }
        if (input == 75)
        {
            return "Nasus";
        }
        if (input == 29)
        {
            return "Twitch";
        }
        if (input == 36)
        {
            return "DrMundo";
        }
        if (input == 61)
        {
            return "Orianna";
        }
        if (input == 28)
        {
            return "Evelynn";
        }
        if (input == 421)
        {
            return "RekSai";
        }
        if (input == 14)
        {
            return "Sion";
        }
        if (input == 164)
        {
            return "Camille";
        }
        if (input == 11)
        {
            return "MasterYi";
        }
        if (input == 13)
        {
            return "Ryze";
        }
        if (input == 54)
        {
            return "Malphite";
        }
        if (input == 34)
        {
            return "Anivia";
        }
        if (input == 98)
        {
            return "Shen";
        }
        if (input == 59)
        {
            return "JarvanIV";
        }
        if (input == 90)
        {
            return "Malzahar";
        }
        if (input == 154)
        {
            return "Zac";
        }
        if (input == 79)
        {
            return "Gragas";
        }        
        else
        {
            return "";
        }     
       
    }
}