# README #

This README would normally document whatever steps are necessary to get your application up and running.
### IMPORTANT INFORMATION ###
● Repository access will be emailed out by Wednesday, Feb 8th.
● There will be a code freeze on Monday, February 20th at 11:59 PM.
● You will be presenting at random on Tuesday, February 21st.
● Presentations are limited to 10 minutes and 5 minutes of questions.
● Check your builds on a fresh computer before the code freeze to ensure a painless
process.
● Very Important: ALL presentation materials MUST be committed to the repository.
● The solution should run as soon as it is pulled down from BitBucket!
● You will present on your own laptops, but we reserve the right to require presentation
from our laptop.

### What is this repository for? ###

*In this phase you will be building mobile app utilizing the League of Legends API. The
app should allow users to search summoners by name, view summoner match history, and
match detail. The match detail should have images for characters and items. You can use this
website for inspiration.
 If you have any questions about how the technologies work, please ask your TAs. If
you need any more clarification, you can always email Envoc at 383@envoc.com.


### Technology Needed ###

● Xamarin
● Git Extensions
● BitBucket

### Contribution guidelines ###
You must sign up for a Riot Developer Account
● You will be building one mobile app, either iOS or Android. You have the option of
targeting Xamarin.iOS, Xamarin.Android, Xamarin.Forms. If you use Xamarin.Forms
only one platform is required.


### Who do I talk to? ###
Nick Dolan at Nicholas.Dolan@selu.edu
or
Envoc at 383@envoc.com